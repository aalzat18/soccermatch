package com.axial.surasoccer

import android.content.Context

class Preferences(context: Context){

    val LOGIN_PREFERENCE = "LoginPreference"
    val IS_LOGGED_IN = "IsLoggedIn"

    val preferences = context.getSharedPreferences(LOGIN_PREFERENCE, Context.MODE_PRIVATE)

    fun getLoginPreference() : Boolean{
        return preferences.getBoolean(IS_LOGGED_IN, false)
    }

    fun setLoginPreference(isLoggedIn : Boolean){
        val editor = preferences.edit()
        editor.putBoolean(IS_LOGGED_IN, isLoggedIn)
        editor.apply()
    }
}