package com.axial.surasoccer

import android.animation.Animator
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Window
import android.view.WindowManager
import kotlinx.android.synthetic.main.activity_splash.*

class SplashActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN)
        setContentView(R.layout.activity_splash)

        animationSplash.addAnimatorListener(object : Animator.AnimatorListener {

            override fun onAnimationCancel(p0: Animator?) {

            }

            override fun onAnimationEnd(p0: Animator?) {
                goToNextActivity()
            }

            override fun onAnimationRepeat(p0: Animator?) {

            }

            override fun onAnimationStart(p0: Animator?) {

            }
        })

    }

    private fun goToNextActivity() {
        val sharedPref = Preferences(this)
        val isLoggedIn = sharedPref.getLoginPreference()

        if (isLoggedIn){
            var intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
        }else{
            var intent = Intent(this, LoginActivity::class.java)
            startActivity(intent)
        }

        finish()
    }
}
