package com.axial.surasoccer

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        login.setOnClickListener{

            if (username.text.toString().equals("ESPN8") && password.text.toString().equals("soccer")){

                goToMainMenu()
                finish()

            }else{
                Toast.makeText(this, "El usuario o la contraseña son incorrectos", Toast.LENGTH_LONG).show()
            }
        }
    }

    fun goToMainMenu(){

        val sharedPref = Preferences(this)
        sharedPref.setLoginPreference(true)

        var intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
    }
}
