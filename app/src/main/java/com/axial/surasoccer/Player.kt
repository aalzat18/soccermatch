package com.axial.surasoccer

data class Player (
    val id: String,
    val name: String,
    val icon: String,
    val x: Float,
    val y: Float
)