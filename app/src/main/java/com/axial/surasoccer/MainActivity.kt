package com.axial.surasoccer

import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MotionEvent
import android.view.View
import android.view.Window
import android.view.WindowManager
import com.google.firebase.database.*
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_main.view.*
import java.lang.Exception


class MainActivity : AppCompatActivity(), ValueEventListener {

    private lateinit var database: DatabaseReference
    val urlImage = "https://firebasestorage.googleapis.com/v0/b/debtor-5f7c6.appspot.com/o/profile%2F"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)
        setContentView(R.layout.activity_main)

        val actionBar = supportActionBar
        actionBar!!.hide()

        database = FirebaseDatabase.getInstance().getReference("player")
        database.addValueEventListener(this)
    }

    private fun createListener(id: String) : View.OnTouchListener {
        return View.OnTouchListener(function = {view, motionEvent ->

            if (motionEvent.action == MotionEvent.ACTION_MOVE){
                view.y = motionEvent.rawY - view.height/2
                view.x = motionEvent.rawX - view.width/2
            }

            database.child(id).child("height").setValue(view.y)
            database.child(id).child("width").setValue(view.x)

            true
        })
    }

    private fun loadPlayers(playersList: ArrayList<Player>) {

        var image1 = urlImage +playersList.get(0).icon+"?alt=media"
        Picasso.get().load(image1).into(iv_player1)
        name_player1.text = playersList.get(0).name

        var image2 = urlImage +playersList.get(1).icon+"?alt=media"
        Picasso.get().load(image2).into(iv_player2)
        name_player2.text = playersList.get(1).name

        var image3 = urlImage +playersList.get(2).icon+"?alt=media"
        Picasso.get().load(image3).into(iv_player3)
        name_player3.text = playersList.get(2).name

        var image4 = urlImage +playersList.get(3).icon+"?alt=media"
        Picasso.get().load(image4).into(iv_player4)
        name_player4.text = playersList.get(3).name

        var image5 = urlImage +playersList.get(4).icon+"?alt=media"
        Picasso.get().load(image5).into(iv_player5)
        name_player5.text = playersList.get(4).name

        var image6 = urlImage +playersList.get(5).icon+"?alt=media"
        Picasso.get().load(image6).into(iv_player6)
        name_player6.text = playersList.get(5).name

        var image7 = urlImage +playersList.get(6).icon+"?alt=media"
        Picasso.get().load(image7).into(iv_player7)
        name_player7.text = playersList.get(6).name

        var image8 = urlImage +playersList.get(7).icon+"?alt=media"
        Picasso.get().load(image8).into(iv_player8)
        name_player8.text = playersList.get(7).name

        var image9 = urlImage +playersList.get(8).icon+"?alt=media"
        Picasso.get().load(image9).into(iv_player9)
        name_player9.text = playersList.get(8).name

        var image10 = urlImage +playersList.get(9).icon+"?alt=media"
        Picasso.get().load(image10).into(iv_player10)
        name_player10.text = playersList.get(9).name
    }

    private fun loadPositions(playersList: ArrayList<Player>) {

        player1.x = playersList.get(0).x
        player1.y = playersList.get(0).y

        player2.x = playersList.get(1).x
        player2.y = playersList.get(1).y

        player3.x = playersList.get(2).x
        player3.y = playersList.get(2).y

        player4.x = playersList.get(3).x
        player4.y = playersList.get(3).y

        player5.x = playersList.get(4).x
        player5.y = playersList.get(4).y

        player6.x = playersList.get(5).x
        player6.y = playersList.get(5).y

        player7.x = playersList.get(6).x
        player7.y = playersList.get(6).y

        player8.x = playersList.get(7).x
        player8.y = playersList.get(7).y

        player9.x = playersList.get(8).x
        player9.y = playersList.get(8).y

        player10.x = playersList.get(9).x
        player10.y = playersList.get(9).y
    }

    private fun setPositionListeners(playersList: ArrayList<Player>) {

        player1.setOnTouchListener(createListener(playersList.get(0).id))
        player2.setOnTouchListener(createListener(playersList.get(1).id))
        player3.setOnTouchListener(createListener(playersList.get(2).id))
        player4.setOnTouchListener(createListener(playersList.get(3).id))
        player5.setOnTouchListener(createListener(playersList.get(4).id))
        player6.setOnTouchListener(createListener(playersList.get(5).id))
        player7.setOnTouchListener(createListener(playersList.get(6).id))
        player8.setOnTouchListener(createListener(playersList.get(7).id))
        player9.setOnTouchListener(createListener(playersList.get(8).id))
        player10.setOnTouchListener(createListener(playersList.get(9).id))
    }

    override fun onCancelled(p0: DatabaseError) {
    }

    override fun onDataChange(snapshot: DataSnapshot) {

        try {
            var playersList = arrayListOf<Player>()

            for (n in 1..snapshot.getChildren().count()) {

                var id = snapshot.child(n.toString()).child("id").value.toString()
                var name = snapshot.child(n.toString()).child("name").value.toString()
                var icon = snapshot.child(n.toString()).child("icon").value.toString()
                var positionX = snapshot.child(n.toString()).child("width").value.toString().toFloat()
                var positionY = snapshot.child(n.toString()).child("height").value.toString().toFloat()

                var player = Player(id, name, icon, positionX, positionY)
                playersList.add(player)

            }

            loadPlayers(playersList)
            loadPositions(playersList)
            setPositionListeners(playersList)

        } catch (exception: Exception) {

            print(exception.message)
        }
    }
}

